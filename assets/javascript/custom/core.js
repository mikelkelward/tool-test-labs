$('.three-slider').slick({
    infinite: true,
    speed: 250,
    slidesToShow: 3,
    slidesToScroll: 1,
    adaptiveHeight: true,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: true
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
});

//$('.is-dropdown-submenu-parent.is-submenu-item').hover(function() {
//    $(this).children('ul').addClass('hide');
//    setTimeout(function() {
//        $(this).children('ul').removeClass('hide');
//    }, 600);
//   //return false;
//});

//$('#menu-top-navigation li.in-drop > ul').addClass('hide');
//$('#menu-top-navigation li.in-drop').hover(
//    function(){
//        // hover code
//        console.log('alright, hovered');
//        console.log($(this).children('ul'));
//        setTimeout(function() {
//            $(this).children('ul').removeClass('hide');
//            console.log('time out');
//        }, 600);
//        return false;
//
//    }, function(){
//        // unhover code
//        setTimeout(function() {
//            $(this).children('ul').addClass('hide');
//        }, 600);
//    }
//);

$('#top-navigation-bar li a').hover(
    function(){
        // hover code
        console.log('alright, hovered');
        $(this).closest('li').addClass('link-active');

    }, function(){
        // unhover code
        $(this).closest('li').removeClass('link-active');
    }
);

$('.woocommerce ul.products li.product .woocommerce .button, .woocommerce-page ul.products li.product .woocommerce .button').text('+');

$(function() {
   $('table.shop_table td.product-name a, table.shop_table td.product-thumbnail a').each(function() {
       $(this).contents().unwrap();
   });

    $('select').select2();

    $('.pagination .prev, .pagination a.next').addClass('round-angle');
    $('.pagination .prev, .pagination a.next').parent().addClass('round-angle-parent');
});

$('select[name="primary-contact"]').empty().prepend( $('select.primary-contact-replacement').children('option') );
$('select[name="state-list"]').empty().prepend( $('select.state-list-replacement').children('option') );
$('input.half').parent().addClass('two-col');

if ('body.page-template-page-faq, body.blog') {
    var pathname = window.location.pathname;
    console.log(pathname);
    $('.sidebar-nav a[href*="'+ pathname +'"]').each(function() {
           $(this).addClass('active');
    });
}