<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "container" div.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/ttl-favicon.ico" type="image/x-icon">
		<!--<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/apple-touch-icon-144x144-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/apple-touch-icon-114x114-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/apple-touch-icon-72x72-precomposed.png">
		<link rel="apple-touch-icon-precomposed" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/apple-touch-icon-precomposed.png"> -->
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
	<?php do_action( 'foundationpress_after_body' ); ?>

	<?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) == 'offcanvas' ) : ?>
	<div class="off-canvas-wrapper">
		<div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
		<?php get_template_part( 'parts/mobile-off-canvas' ); ?>
	<?php endif; ?>

	<?php do_action( 'foundationpress_layout_start' ); ?>

	<header id="masthead" class="site-header bg-gray" role="banner">
		<div class="title-bar" data-responsive-toggle="site-navigation">
			<button class="menu-icon" type="button" data-toggle="mobile-menu"></button>

		</div>

		<nav id="site-navigation" class="main-navigation top-bar show-for-small-only" role="navigation" style="display: none">
			<div class="top-bar-left">
				<ul class="menu">
					<li class="home"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></li>
				</ul>
			</div>
			<div class="top-bar-right">
				<?php foundationpress_top_bar_r(); ?>

				<?php if ( ! get_theme_mod( 'wpt_mobile_menu_layout' ) || get_theme_mod( 'wpt_mobile_menu_layout' ) == 'topbar' ) : ?>
					<?php get_template_part( 'parts/mobile-top-bar' ); ?>
				<?php endif; ?>
			</div>
		</nav>
		<nav id="site-nav">
			<div class="nav-wrap row">
				<div class="small-5 medium-4 columns">
					<a href="<?php echo site_url(); ?>" class="logo">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/tooltestinglab-logo.png" alt="Tool Test Lab">
					</a>
				</div>
				<div class="small-8 hide-for-small-only columns text-right">
					<div class="top-links">
						<?php if( have_rows('header_links', 'option') ): ?>

							<?php while( have_rows('header_links', 'option') ): the_row();

								// vars
								$hlink_title = get_sub_field('header_link_title', 'header_links');
								$hlink_url = get_sub_field('header_link_url', 'header_links');
								?>

								<a href="<?php echo $hlink_url; ?>" title="<?php echo $hlink_title; ?>"><?php echo $hlink_title; ?></a>

							<?php endwhile; ?>

						<?php endif; ?>

					</div>
					<div class="social-headwrap">
						<a href="<?php echo site_url(); ?>/get-a-quote" class="button minute dark up request-quote">Request a Quote</a>
						<?php if( have_rows('social_media', 'option') ): ?>

						<ul class="social-btns inline-list">
							<?php while( have_rows('social_media', 'option') ): the_row();

								// vars
								$social_icon = get_sub_field('social_media_icon', 'option');
								$social_link = get_sub_field('social_media_link', 'option');
								$shift_social = get_sub_field('shift_icon', 'option');
								?>

								<li><a class="button icon-btn round<?php if( $shift_social ) { echo ' shift'; } ?>" href="<?php echo $social_link; ?>" target="_blank"><i class="fa fa-<?php echo $social_icon; ?>"></i></a></li>

							<?php endwhile; ?>
						</ul>

						<?php endif; ?>
					</div>
				</div>
			</div>
			<div class="row show-for-medium">
				<div class="small-12 columns">
					<div id="top-navigation-bar" class="clearfix">
						<?php foundationpress_top_bar_r(); ?>
<!--						<a class="search-site right button icon-btn round shift" href="#"><i style="left: -3px;" class="fa fa-search"></i></a>-->
						<form class="search-container" action="<?php echo home_url('/'); ?>" method="get" role="search">
							<input id="search-box" type="text" placeholder="Search&hellip;" class="search-box" name="s" />
							<label for="search-box"><i class="fa fa-search"></i></label>
							<input type="submit" id="search-submit" />
						</form>
					</div>
				</div>
			</div>

			<?php get_template_part( 'parts/modules/page-header' ); ?>

			<?php if (is_page_template('page.php') || is_page_template('templates/page-full-width.php') || is_page_template('templates/page-sidebar-left.php')) {
				get_template_part( 'parts/modules/request-quote-intro' );
				get_template_part( 'parts/modules/3-block-tile' );
			} ?>

		</nav>
	</header>

	<section class="container">
		<?php do_action( 'foundationpress_after_header' ); ?>
