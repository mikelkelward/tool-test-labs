<?php
/**
 * Enqueue all styles and scripts
 *
 * Learn more about enqueue_script: {@link https://codex.wordpress.org/Function_Reference/wp_enqueue_script}
 * Learn more about enqueue_style: {@link https://codex.wordpress.org/Function_Reference/wp_enqueue_style }
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

if ( ! function_exists( 'foundationpress_scripts' ) ) :
	function foundationpress_scripts() {

	// Enqueue the main Stylesheet.
	wp_enqueue_style( 'site-fonts', 'https://fonts.googleapis.com/css?family=Titillium+Web:400,300,600,700', array(), '1.0', 'all' );

	// Deregister the jquery version bundled with WordPress.
	wp_deregister_script( 'jquery' );

	// CDN hosted jQuery placed in the header, as some plugins require that jQuery is loaded in the header.
	wp_enqueue_script( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js', array(), '2.1.0', false );
	wp_enqueue_script( 'sel', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js', array(), '4.0.2', false );
	wp_enqueue_script( 'slick-js', '//cdn.jsdelivr.net/jquery.slick/1.5.9/slick.min.js', array(), '2.1.0', false );
	wp_enqueue_style( 'slick-css', '//cdn.jsdelivr.net/jquery.slick/1.5.9/slick.css', array(), '2.1.0', false );
	wp_enqueue_style( 'slick-theme-css', '//cdn.jsdelivr.net/jquery.slick/1.5.9/slick-theme.css', array(), '2.1.0', false );
	wp_enqueue_style( 'sel-css', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css', array(), '4.0.2', false );
	wp_enqueue_style( 'ficons-css', 'http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css', array(), '4.0.2', false );

	// New WooCommerce Stylesheets
//	wp_enqueue_style( 'new-woocommerce-layout', get_stylesheet_directory_uri() . '/assets/stylesheets/woo/woocommerce-layout.css', array(), WC_VERSION, false );
//	wp_enqueue_style( 'new-woocommerce-smallscreen', get_stylesheet_directory_uri() . '/assets/stylesheets/woo/woocommerce-smallscreen.css', array(), WC_VERSION, 'only screen and (max-width: ' . apply_filters( 'woocommerce_style_smallscreen_breakpoint', $breakpoint = '768px' ) . ')');
//	wp_enqueue_style( 'new-woocommerce-general', get_stylesheet_directory_uri() . '/assets/stylesheets/woo/woocommerce.css', array(), WC_VERSION, false );

	// If you'd like to cherry-pick the foundation components you need in your project, head over to gulpfile.js and see lines 35-54.
	// It's a good idea to do this, performance-wise. No need to load everything if you're just going to use the grid anyway, you know :)
	wp_enqueue_script( 'foundation', get_template_directory_uri() . '/assets/javascript/foundation.js', array('jquery'), '2.3.0', true );

	add_image_size( 'service-tile', 600, 400, array( 'left', 'top' ) );
	add_image_size( 'product-md', 600, 600, array( 'left', 'top' ) );

	// Add the comment-reply library on pages where it is necessary
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	}

	add_action( 'wp_enqueue_scripts', 'foundationpress_scripts' );
endif;


function footer_scripts() { ?>
	<script type='text/javascript' id="__bs_script__">//<![CDATA[
		document.write("<script async src='http://HOST:3001/browser-sync/browser-sync-client.2.11.1.js'><\/script>".replace("HOST", location.hostname));
		//]]></script>
<?php }
add_action('foundationpress_before_closing_body', 'footer_scripts');

function shop_sku(){
	global $product;
	echo '<span itemprop="productID" class="sku">' . $product->sku . '</span>';
}
add_action( 'woocommerce_before_shop_loop_item_title', 'shop_sku' );

// Disable default WooCommerce stylesheet
//add_filter( 'woocommerce_enqueue_styles', '__return_false');

function enqueue_style_after_wc(){
	$deps = class_exists( 'WooCommerce' ) ? array( 'woocommerce-layout', 'woocommerce-smallscreen', 'woocommerce-general' ) : array();
	wp_enqueue_style( 'main-stylesheet', get_stylesheet_directory_uri() . '/assets/stylesheets/foundation.css', array(), '2.3.0', 'all' );
}
add_action( 'wp_enqueue_scripts', 'enqueue_style_after_wc' );

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
			'page_title' 	=> 'Tool Test Labs Site General Settings',
			'menu_title'	=> 'Theme Settings',
			'menu_slug' 	=> 'theme-general-settings',
			'capability'	=> 'edit_posts',
			'redirect'		=> false
	));

	acf_add_options_sub_page(array(
			'page_title' 	=> 'Sidebar Menus',
			'menu_title'	=> 'Sidebar Menus',
			'parent_slug'	=> 'theme-general-settings',
	));
}

function get_excerpt($count){
//	$permalink = get_permalink($post->ID);
	$excerpt = get_the_excerpt();
	$excerpt = strip_tags($excerpt);
	$excerpt = substr($excerpt, 0, $count);
	$excerpt = $excerpt.'&hellip;';
//	$excerpt = $excerpt.'... <a href="'.$permalink.'">more</a>';
	return $excerpt;
}

//add_shortcode( 'product_categories_dropdown', 'woo_product_categories_dropdown' );
function woo_product_categories_dropdown( $atts ) {
	extract(shortcode_atts(array(
			'count'         => '0',
			'hierarchical'  => '0',
			'orderby' 	    => ''
	), $atts));

	ob_start();

	$c = array();
	$h = 1;
	$o = ( isset( $orderby ) && $orderby != '' ) ? $orderby : 'order';

	// Stuck with this until a fix for http://core.trac.wordpress.org/ticket/13258
	wc_product_dropdown_categories( $c, $h, 0, $o );
	?>
	<script type='text/javascript'>
		/* <![CDATA[ */
		var product_cat_dropdown = document.getElementById("dropdown_product_cat");
		function onProductCatChange() {
			if ( product_cat_dropdown.options[product_cat_dropdown.selectedIndex].value !=='' ) {
				location.href = "<?php echo home_url(); ?>/?product_cat="+product_cat_dropdown.options[product_cat_dropdown.selectedIndex].value;
			}
		}
		product_cat_dropdown.onchange = onProductCatChange;
		/* ]]> */
	</script>
	<?php

	return ob_get_clean();

}

//add_action('woocommerce_before_shop_loop', 'woo_cat_dropdown', 35);
function woo_cat_dropdown() {
	echo do_shortcode('[product_categories_dropdown orderby="title" count="0" hierarchical="0"]');
}

?>
