<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

		</section>
		<div id="footer-container">
			<footer id="footer" class="row">
				<?php do_action( 'foundationpress_before_footer' ); ?>

				<div class="show-for-small-only columns">
					<div class="row collapse">
						<?php //dynamic_sidebar( 'footer-widget-1' ); ?>
						<?php //dynamic_sidebar( 'footer-widget-2' ); ?>
						<?php dynamic_sidebar( 'footer-widget-4' ); ?>
						<article class="small-12 medium-3 columns">
							<?php echo bloginfo('name'); ?><br>
							<?php echo get_field('street_address', 'option'); ?><br>
							<?php echo get_field('city_state_zip', 'option'); ?><br><br>
							Phone: <?php echo get_field('phone_number', 'option'); ?><br>
							Fax: <?php echo get_field('fax_number', 'option'); ?>
						</article>
						<?php dynamic_sidebar( 'footer-widget-3' ); ?>
						<article class="small-12 medium-3 columns">
							&copy; 2010-<?php echo date('Y'); ?><br>
							<?php echo bloginfo('name'); ?>, Inc.<br>
							All Rights Reserved.
						</article>
					</div>
				</div>

				<div class="show-for-medium columns">
					<div class="row collapse">
						<?php //dynamic_sidebar( 'footer-widget-1' ); ?>
						<article class="small-12 medium-3 columns">
							&copy; 2010-<?php echo date('Y'); ?><br>
							<?php echo bloginfo('name'); ?>, Inc.<br>
							All Rights Reserved.
						</article>
						<?php //dynamic_sidebar( 'footer-widget-2' ); ?>
						<article class="small-12 medium-3 columns">
							<?php echo bloginfo('name'); ?><br>
							<?php echo get_field('street_address', 'option'); ?><br>
							<?php echo get_field('city_state_zip', 'option'); ?><br><br>
							Phone: <?php echo get_field('phone_number', 'option'); ?><br>
							Fax: <?php echo get_field('fax_number', 'option'); ?>
						</article>
						<?php dynamic_sidebar( 'footer-widget-3' ); ?>
						<?php dynamic_sidebar( 'footer-widget-4' ); ?>
					</div>
				</div>
				<?php do_action( 'foundationpress_after_footer' ); ?>
			</footer>
		</div>

		<?php do_action( 'foundationpress_layout_end' ); ?>

<?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) == 'offcanvas' ) : ?>
		</div><!-- Close off-canvas wrapper inner -->
	</div><!-- Close off-canvas wrapper -->
</div><!-- Close off-canvas content wrapper -->
<?php endif; ?>

<?php wp_footer(); ?>
<?php do_action( 'foundationpress_before_closing_body' ); ?>
</body>
</html>
