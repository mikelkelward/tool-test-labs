<?php
/*
Template Name: Contact
*/
get_header(); ?>

<?php get_template_part( 'parts/featured-image' ); ?>

<div id="page-sidebar-left" role="main">

<div class="row">
    <div class="small-12 medium-4 large-4 columns side-menu">
        <div class="contact-sidebar">
            <div class="contact-bar">Contact Us At:</div>
            <div class="side-content">
                <div class="row">
                    <div class="small-6 medium-12 columns">
                        <p><?php echo get_field('street_address', 'option'); ?><br>
                            <?php echo get_field('city_state_zip', 'option'); ?></p>
                        <p>Phone: <?php echo get_field('phone_number', 'option'); ?><br>
                            Fax: <?php echo get_field('fax_number', 'option'); ?></p>
                    </div>
                    <div class="small-6 medium-12 columns">
                        <?php if( have_rows('lead_positions', 'option') ): ?>

                            <?php while( have_rows('lead_positions', 'option') ): the_row();
                                // vars
                                $contact_name = get_sub_field('contact_persons_name', 'option');
                                $position_title = get_sub_field('position_title', 'option');
                                ?>
                                <p>
                                    <strong><?php echo $position_title; ?></strong><br>
                                    <?php echo $contact_name; ?>
                                </p>
                            <?php endwhile; ?>

                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="small-12 medium-8 large-pull-1 columns side-menu">
        <?php do_action( 'foundationpress_before_content' ); ?>
        <?php while ( have_posts() ) : the_post(); ?>
            <article <?php post_class('close-sidebar') ?> id="post-<?php the_ID(); ?>">
                <?php if(get_field('page_subtitle')): ?>
                <header>
                    <h2 class="text-center h1 entry-title"><?php the_field('page_subtitle'); ?></h2>
                </header>
                <?php endif; ?>
                <?php do_action( 'foundationpress_page_before_entry_content' ); ?>
                <div class="entry-content">
                    <?php the_content(); ?>
                    <?php get_template_part( 'parts/modules/accordion' ); ?>
                    <?php get_template_part( 'parts/modules/field-replacements' ); ?>
                </div>
                <footer>
                    <?php wp_link_pages( array('before' => '<nav id="page-nav"><p>' . __( 'Pages:', 'foundationpress' ), 'after' => '</p></nav>' ) ); ?>
                    <p><?php the_tags(); ?></p>
                </footer>
                <?php do_action( 'foundationpress_page_before_comments' ); ?>
                <?php comments_template(); ?>
                <?php do_action( 'foundationpress_page_after_comments' ); ?>
            </article>
        <?php endwhile;?>

        <?php do_action( 'foundationpress_after_content' ); ?>
    </div>
</div>
<?php //get_sidebar(); ?>

</div>

<?php get_footer(); ?>
