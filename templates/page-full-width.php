<?php
/*
Template Name: Full Width
*/
get_header(); ?>

<?php get_template_part( 'parts/featured-image' ); ?>

<div class="bg-light">
    <div id="page-full-width" role="main">

    <?php do_action( 'foundationpress_before_content' ); ?>
    <?php while ( have_posts() ) : the_post(); ?>
      <article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
          <header>
              <h2 class="text-center h1 entry-title"><?php the_field('page_subtitle'); ?></h2>
          </header>
          <?php do_action( 'foundationpress_page_before_entry_content' ); ?>
          <div class="row">
              <div class="entry-content small-12 medium-10 medium-centered">
                  <?php the_content(); ?>
                  <?php get_template_part( 'parts/modules/accordion' ); ?>
              </div>
          </div>
          <footer>
              <?php wp_link_pages( array('before' => '<nav id="page-nav"><p>' . __( 'Pages:', 'foundationpress' ), 'after' => '</p></nav>' ) ); ?>
              <p><?php the_tags(); ?></p>
          </footer>
          <?php do_action( 'foundationpress_page_before_comments' ); ?>
          <?php comments_template(); ?>
          <?php do_action( 'foundationpress_page_after_comments' ); ?>

          <?php get_template_part( 'parts/modules/field-replacements' ); ?>
      </article>
    <?php endwhile;?>

    <?php do_action( 'foundationpress_after_content' ); ?>

    </div>
</div>

<?php get_template_part( 'parts/modules/dark-cta-block' ); ?>

<?php get_footer(); ?>
