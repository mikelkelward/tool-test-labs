<?php
/*
Template Name: FAQ
*/
get_header(); ?>

<?php get_template_part( 'parts/featured-image' ); ?>

<div class="bg-light">
    <div id="faq-content" class="content-case" role="main">

        <div class="row">
            <div class="small-12 medium-4 large-4 columns side-menu">
                <div class="faq-sidebar">
                    <div class="contact-bar"><?php echo get_field('faq_sidemenu_title', 'option'); ?></div>
                    <div class="side-content">
                        <div class="row">
                            <div class="small-6 medium-12 columns sidebar-nav">
                                <?php echo get_field('faq_sidebar_content', 'option'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="small-12 medium-8 large-pull-1 columns side-menu">
                <?php do_action( 'foundationpress_before_content' ); ?>
                <?php while ( have_posts() ) : the_post(); ?>
                    <article <?php post_class('close-sidebar') ?> id="post-<?php the_ID(); ?>">
                        <?php if(get_field('page_subtitle')): ?>
                            <header>
                                <h2 class="text-center h1 entry-title"><?php the_field('page_subtitle'); ?></h2>
                            </header>
                        <?php endif; ?>
                        <?php do_action( 'foundationpress_page_before_entry_content' ); ?>
                        <div class="entry-content">
                            <?php the_content(); ?>
                            <?php get_template_part( 'parts/modules/accordion-faq' ); ?>
                        </div>
                        <footer>
                            <?php wp_link_pages( array('before' => '<nav id="page-nav"><p>' . __( 'Pages:', 'foundationpress' ), 'after' => '</p></nav>' ) ); ?>
                            <p><?php the_tags(); ?></p>
                        </footer>
                        <?php do_action( 'foundationpress_page_before_comments' ); ?>
                        <?php comments_template(); ?>
                        <?php do_action( 'foundationpress_page_after_comments' ); ?>
                    </article>
                <?php endwhile;?>

                <?php do_action( 'foundationpress_after_content' ); ?>
            </div>
        </div>
        <?php //get_sidebar(); ?>

    </div>
</div>

<?php get_footer(); ?>
