<?php
/*
Template Name: Front
*/
get_header(); ?>

<div class="bg-gray">
	<div class="row small-collapse medium-uncollapse">
		<div class="small-12 columns">
			<div class="hero-wrap">
				<div class="img-wrap"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/miss-your-tools-herobg.png" alt=""></div>
				<div class="row">
					<div class="small-12 columns">
						<div class="cover-copy">
							<div class="cover-text"><?php echo get_field('hero_title')?></div>
							<a href="<?php echo get_field('hero_button_url')?>" class="button primary big"><?php echo get_field('hero_button_text')?></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="small-12 medium-10 medium-centered columns">
			<div class="three-slider">
				<?php if( have_rows('home_slides') ): ?>

					<?php while( have_rows('home_slides') ): the_row();

						// vars
						$hslide_title = get_sub_field('hslide_title');
						$hslide_link = get_sub_field('slide_page_link');
						$hslide_image = get_sub_field('hslide_image');
						?>

						<div>
							<a href="<?php echo $hslide_link; ?>">
								<?php
								if( !empty($hslide_image) ): ?>
									<img src="<?php echo $hslide_image['url']; ?>" alt="<?php echo $hslide_image['alt']; ?>" />
								<?php endif; ?>
								<span class="slide-title"><?php echo $hslide_title; ?></span>
							</a>
						</div>

					<?php endwhile; ?>

				<?php endif; ?>
			</div>
		</div>
	</div>
</div>

<?php do_action( 'foundationpress_before_content' ); ?>

<?php do_action( 'foundationpress_after_content' ); ?>

<?php get_template_part('parts/modules/4-block-tile'); ?>

<?php get_template_part('parts/modules/email-bar'); ?>

<?php get_template_part('parts/modules/3-block-tile'); ?>

<div class="bg-light content-case">
	<div class="row">
		<div class="small-12 columns text-center">
			<img class="pad-b" src="<?php echo get_template_directory_uri(); ?>/assets/images/quote-icon.png" alt="">
			<h2 class="h1"><strong>Precision</strong> Calibration, Certification and Sales</h2>
			<a href="<?php echo site_url(); ?>/get-a-quote" class="button primary green big space">Request a Quote</a>
		</div>
	</div>
</div>

<?php get_footer(); ?>