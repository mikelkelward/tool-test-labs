<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

<div id="post-<?php the_ID(); ?>" <?php post_class('blogpost-entry row collapse'); ?> data-equalizer="post-height">
	<?php
	$thumb_id = get_post_thumbnail_id();
	$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
	$thumb_url = $thumb_url_array[0];

	if(has_post_thumbnail()) { ?>
	<div class="post-img small-3 medium-4 columns" style="background: url(<?php echo $thumb_url; ?>) no-repeat; background-size: cover" data-equalizer-watch="post-height"></div>
	<?php } ?>
	<div class="post-copy-wrap <?php if(has_post_thumbnail()) { ?>small-9 medium-8<?php } else { ?>small-12<?php } ?> columns" data-equalizer-watch="post-height">
		<div class="post-copy">
			<header>
				<h6><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h6>
				<?php //foundationpress_entry_meta(); ?>
			</header>
			<div class="entry-content">
				<?php //the_content( __( 'Continue reading...', 'foundationpress' ) ); ?>
				<?php //the_excerpt( __( 'Continue reading...', 'foundationpress' ) ); ?>
				<p class="hide-for-small-only"><?php echo get_excerpt(310); ?></p>
				<p class="show-for-small-only"><?php echo get_excerpt(170); ?></p>
			</div>
			<div class="arrow-link show-for-medium">
				<a href="<?php the_permalink(); ?>" class="round-angle"><i class="ion-ios-arrow-right"></i></a>
			</div>
			<footer>
				<?php $tag = get_the_tags(); if ( $tag ) { ?><p><?php the_tags(); ?></p><?php } ?>
			</footer>
		</div>
	</div>
</div>
