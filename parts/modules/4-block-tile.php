<?php

if( get_field('display_4_tile_block') ) { ?>

<?php if( have_rows('4_service_block') ): ?>

<div id="four-service-block" class="bg-light content-case">
	<div class="row" data-equalizer="service-block-4" data-equalizer-mq="">
		<div class="small-12 columns">
			<h2 class="h1 text-center"><?php the_field('4_tile_section_title'); ?></h2>
		</div>

		<?php while( have_rows('4_service_block') ): the_row();

			// vars
			$service_icon4 = get_sub_field('service_icon_four');
			$service_title4 = get_sub_field('service_title');
			$service_desc4 = get_sub_field('service_description');
			$service_link4 = get_sub_field('service_button_link');
			?>

			<div class="medium-6 large-3 columns service-tile">
				<?php if( $service_icon4 ) { ?>
					<span class="service-icon"><i class="fa fa-<?php echo $service_icon4; ?>"></i></span>
				<?php } else { ?>
					<span class="service-icon"><i class="fa fa-cog"></i></span>
				<?php } ?>

				<h3 class="h3 s-title"><?php echo $service_title4; ?></h3>
				<p class="s-desc" data-equalizer-watch="service-block-4"><?php echo $service_desc4; ?></p>
				<a href="<?php echo $service_link4; ?>" class="button primary blue">Learn More</a>
			</div>

		<?php endwhile; ?>
	</div>
</div>

<?php endif; ?>

<?php }