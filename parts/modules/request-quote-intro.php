<?php if( get_field('display_request_quote_steps') ): ?>
	<?php if( have_rows('request_quote_steps') ): ?>

	<div class="dark-cta-block bg-gray content-case overlap">
	  <div class="row">

	  <div class="small-12 medium-10 medium-centered columns">
		<?php while( have_rows('request_quote_steps') ): the_row();

		  // vars
			$raq_title = get_sub_field('raq_title');
			$raq_img = get_sub_field('request_quote_step_img');
		  ?>

		  <div class="cta-tile small-12 medium-4 columns">
			  <?php
			  if( !empty($raq_img) ): ?>
				  <span class="cta-icon"><img src="<?php echo $raq_img['url']; ?>" alt="<?php echo $raq_img['alt']; ?>" /></span>
			  <?php endif; ?>
			  <span class="slide-title wide"><?php echo $raq_title; ?></span>
		  </div>

		<?php endwhile; ?>
	  </div>

	  </div>
	</div>

	<?php endif; ?>
<?php endif; ?>