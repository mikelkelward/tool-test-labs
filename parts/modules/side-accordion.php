	<?php if( have_rows('newsfeed_sidemenu', 'option') ): $count = 0; ?>

			<h5 class="submenu-title">Sub Menu</h5>
			<ul class="accordion side-accordion-menu sidebar-nav" data-accordion>
					<?php while( have_rows('newsfeed_sidemenu', 'option') ): the_row();

						// vars
						$side_ac_title = get_sub_field('side_accordion_title', 'option');
						$side_ac_content = get_sub_field('side_accordion_content', 'option');
						?>

						<li class="accordion-item <?php if (!$count) { ?>is-active<?php } ?>" data-accordion-item>
							<a href="#" class="accordion-title"><?php echo $side_ac_title; ?></a>
							<div class="accordion-content" data-tab-content>
								<?php echo $side_ac_content; ?>
							</div>
						</li>

					<?php $count++; endwhile; ?>
			</ul>

	<?php endif; ?>