<div class="email-bar">
  <div class="row">
	<form action="#">
	  <div class="small-12 medium-10 medium-centered columns">
		<div class="row">
		  <div class="hide-for-small-only small-8 medium-9 columns">
			<div class="icon-field">
			  <input type="text" class="pre-icon" placeholder="Enter your email address to subscribe to our newsletter!">
			  <span class="button icon-btn round shift more white"><i class="fa fa-envelope text-white"></i></span>
			</div>
		  </div>
		  <div class="show-for-small-only columns">
			<div class="icon-field text-center">
			  <h3 class="h2 text-white">Subscribe to Our <strong>Newsletter!</strong></h3>
			</div>
		  </div>
		  <div class="small-12 medium-3 columns text-center medium-text-left">
			<a href="#" class="submit button secondary big">Sign Up</a>
		  </div>
		</div>
	  </div>
	</form>
  </div>
</div>