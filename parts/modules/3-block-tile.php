<?php

if( get_field('display_3_featured') ) { ?>

<?php if( have_rows('3_featured_tiles_block') ): ?>

<div id="three-service-block" class="bg-gray content-case">
	<div class="row" data-equalizer="service-block" data-equalizer-mq="medium-up">
		<?php while( have_rows('3_featured_tiles_block') ): the_row();

			// vars
			$service_title3 = get_sub_field('featured_service_title_3');
			$service_desc3 = get_sub_field('featured_service_description_3');
			$service_link3 = get_sub_field('service_tile_page_link_3');
			$service_image = get_sub_field('stile_image');
			?>

			<div class="small-11 small-centered medium-6 medium-uncentered large-4 columns service-tile">
				<?php
				if( !empty($service_image) ): ?>
					<img src="<?php echo $service_image['url']; ?>" alt="<?php echo $service_image['alt']; ?>" />
				<?php endif; ?>
				<h3 class="h2 s-title text-light"><?php echo $service_title3; ?></h3>
				<p class="s-desc text-white" data-equalizer-watch="service-block"><?php echo $service_desc3; ?></p>
				<a href="<?php echo $service_link3; ?>" class="button secondary blue big">Learn More</a>
			</div>

		<?php endwhile; ?>
	</div>
</div>

<?php endif; ?>

<?php } ?>
