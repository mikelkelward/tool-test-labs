<?php

$queried_object = get_queried_object();
$taxonomy = $queried_object->taxonomy;
$term_id = $queried_object->term_id;

$term = $wp_query->queried_object;

if( get_field('page_title_banner') || get_field('page_title_banner', 'product_cat_' . $term_id) ) { ?>

  <div id="page-title-wrap" class="row">
	<?php $image = get_field('banner_background');
	if( !empty($image) ) { ?>
	<div class="page-title-banner small-12 columns dot">
	<div style="background: url(<?php echo $image['url']; ?>)"></div>
<!--	<img src="--><?php //echo $image['url']; ?><!--" alt="--><?php //echo $image['alt']; ?><!--" data-equalizer-watch="title-banner" />-->
	<?php } else { ?>
	<div class="page-title-banner small-12 columns">
	<div style="background: url(<?php echo get_template_directory_uri(); ?>/assets/images/main-banner-bg.png);"></div>
<!--	<img src="--><?php //echo get_template_directory_uri(); ?><!--/assets/images/main-banner-bg.png" alt="" data-equalizer-watch="title-banner">-->
	<?php } ?>
		<?php if (is_product_category()) : ?>
			<h1 class="page-btitle text-center"><?php echo $term->name; ?></h1>
		<?php else: ?>
			<h1 class="page-btitle text-center"><?php if (get_field('page_otitle')) { the_field('page_otitle'); } else { the_title(); };?></h1>
		<?php endif; ?>
	</div>
  </div>

<?php }