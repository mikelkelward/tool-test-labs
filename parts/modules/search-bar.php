<div class="row">
  <form class="search" method="get" action="<?php echo home_url('/'); ?>">
	<div class="small-12 medium-10 large-8 medium-centered columns">
	  <div class="row">
		<div class="small-8 medium-9 columns">
		  <div class="icon-field">
			<input type="text" class="pre-icon" name="s" id="s" placeholder="Enter search criteria&hellip;">
			<span class="button icon-btn round shift text-blue"><i class="fa fa-search"></i></span>
		  </div>
		</div>
		<div class="small-4 medium-3 columns">
		  <input type="submit" value="Search" class="submit button secondary blue big show-for-small-only">
		  <input type="submit" value="Search" class="submit button secondary blue big sm-width show-for-medium">
		</div>
	  </div>
	</div>
  </form>
</div>