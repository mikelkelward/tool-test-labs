<?php

$queried_object = get_queried_object();
$taxonomy = $queried_object->taxonomy;
$term_id = $queried_object->term_id;

if( get_field('display_3_featured_products', 'product_cat_'. $term_id) ) { ?>

<?php if( have_rows('3_featured_products_block', 'product_cat_'. $term_id) ): ?>

<div id="three-service-block" class="bg-gray content-case head">
	<?php get_template_part('parts/modules/search-bar'); ?>
	<div class="row" data-equalizer="service-block" data-equalizer-mq="medium-up">
		<?php while( have_rows('3_featured_products_block', 'product_cat_'. $term_id) ): the_row();

			// vars
			$product_title3 = get_sub_field('featured_product_title_3');
			$product_desc3 = get_sub_field('featured_product_description_3');
			$product_link3 = get_sub_field('product_tile_page_link_3');
			$product_image = get_sub_field('product_image_3');
			?>

			<div class="small-11 small-centered medium-6 medium-uncentered large-4 columns text-center product-tile">
				<?php
				if( !empty($product_image) ): ?>
					<img src="<?php echo $product_image['url']; ?>" alt="<?php echo $product_image['alt']; ?>" />
				<?php endif; ?>
				<h3 class="h2 s-title text-light"><?php echo $product_title3; ?></h3>
				<p class="s-desc text-white" data-equalizer-watch="product-block"><?php echo $product_desc3; ?></p>
				<a href="<?php echo $product_link3; ?>" class="button secondary blue big">Learn More</a>
			</div>

		<?php endwhile; ?>
	</div>
</div>

<?php endif; ?>

<?php } ?>
