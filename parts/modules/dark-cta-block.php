<?php if( get_field('display_dark_call_to_actions') ): ?>
<?php if( have_rows('dark_call_to_actions') ): ?>

<div class="dark-cta-block bg-gray content-case">
  <div class="row">

  <div class="small-12 medium-10 medium-centered columns">
	<?php while( have_rows('dark_call_to_actions') ): the_row();

	  // vars
	  $dlink_title = get_sub_field('dlink_title');
	  $dlink_link = get_sub_field('dlink_url');
	  $dlink_icon = get_sub_field('font_icon');
	  $dlink_sectiontitle = get_field('dark_section_title');
	  ?>

	  <div class="cta-tile small-12 medium-4 columns">
		<a href="<?php echo $dlink_link; ?>">
		  <?php if( $dlink_icon ) { ?>
			<span class="cta-icon"><i class="fa fa-<?php echo $dlink_icon; ?>"></i></span>
		  <?php } else { ?>
			<span class="cta-icon"><i class="fa fa-cog"></i></span>
		  <?php } ?>
		  <span class="slide-title"><?php echo $dlink_title; ?></span>
		</a>
	  </div>

	<?php endwhile; ?>
  </div>

  </div>
	<div class="row">
		<div class="small-12 text-center columns">
			<h3 class="h1 text-white"><?php echo $dlink_sectiontitle; ?></h3>
			<?php get_template_part('parts/modules/search-bar'); ?>
		</div>
	</div>
</div>

<?php endif; ?>
<?php endif; ?>