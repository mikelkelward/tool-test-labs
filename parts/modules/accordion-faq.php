<?php if( get_field('display_accordion_faq') ): ?>
	<?php if( have_rows('faq_accordion') ): $count = 0; ?>

			<ul class="accordion faq sidebar-nav" data-accordion>
					<?php while( have_rows('faq_accordion') ): the_row();

						// vars
						$faq_title = get_sub_field('accordion_faq_title');
						$faq_content = get_sub_field('accordion_faq_content');
						?>

						<li class="accordion-item <?php if (!$count) { ?>is-active<?php } ?>" data-accordion-item>
							<a href="#" class="accordion-title"><?php echo $faq_title; ?></a>
							<div class="accordion-content" data-tab-content>
								<?php echo $faq_content; ?>
							</div>
						</li>

					<?php $count++; endwhile; ?>
			</ul>

	<?php endif; ?>
<?php endif; ?>