<?php if( get_field('display_accordion') ): ?>
	<?php if( have_rows('dyn_accordion') ): $count = 0; ?>

			<ul class="accordion" data-accordion>
					<?php while( have_rows('dyn_accordion') ): the_row();

						// vars
						$ac_title = get_sub_field('accordion_title');
						$ac_content = get_sub_field('accordion_content');
						?>

						<li class="accordion-item <?php if (!$count) { ?>is-active<?php } ?>" data-accordion-item>
							<a href="#" class="accordion-title"><?php echo $ac_title; ?></a>
							<div class="accordion-content sidebar-nav" data-tab-content>
								<?php echo $ac_content; ?>
							</div>
						</li>

					<?php $count++; endwhile; ?>
			</ul>

	<?php endif; ?>
<?php endif; ?>
